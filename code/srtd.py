# Implement SRTD algorithm for Oldroyd 3 model

from dolfin import *
import numpy as np

# Make a mesh
mesh = UnitSquareMesh(32, 32, "crossed")

# Make function spaces
pressure = FunctionSpace(mesh, "Lagrange", 3)
velocity = VectorFunctionSpace(mesh, "Lagrange", 4)
stress = TensorFunctionSpace(mesh, "Lagrange", 3)

# Define lid-driven cavity boundary
noslip  = DirichletBC(velocity, Constant((0, 0)), "x[0] < DOLFIN_EPS || x[0] > 1.0 - DOLFIN_EPS || x[1] < DOLFIN_EPS")
lid  = DirichletBC(velocity, Constant((1,0)), "x[1] > 1.0 - DOLFIN_EPS")
bcs = [noslip, lid] 

# Define trial functions
u = Function(velocity)
p = Function(pressure)
T = Function(stress)

# By defining the function in this step and omitting the trial function we tell FEniCS that the problem is nonlinear. 

# Define test functions
v = TestFunction(velocity)
q = TestFunction(pressure)
U = TestFunction(stress)

# Other functions
ulast = Function(velocity)
pi = Function(pressure)
f = Constant((0, 0))

# Define parameters
nu, lambda1, mu1 = 1, 1, 1
r = 1.0e3

def E(u): 
	return 0.5*(grad(u) + grad(u).T)

def R(u):
	return 0.5*(grad(u).T - grad(u))

def F(f, u, p, pi, T):
	return dot(f, v - lambda1*dot(u, grad(v))) - lambda1*p*inner(grad(u).T, grad(v)) - dot(dot(u, grad(u)), dot(u, grad(v))) + inner(grad(u)*T, grad(v)) + (lambda1 - mu1)*inner(E(u)*T + T*E(u), grad(v))

def M(u, T):
	return R(u)*T + T*(R(u).T) - mu1*(E(u)*T + T*E(u))

# SRTD algorithm: 
# 0. Start: u = 0, p = 0, T = 0
# 1. Loop:
#	 2. Use Unified Stokes to find u and pi
#	 	Unified Stokes algorithm:
#	 	0. Start: u = 0, pi = 0
# 	 	1. Loop:
#	 		2. Find u such that F1 = 0 for all v
#	 		3. Find pi with update
# 	3. Find p such that F2 = 0 for all q
# 	4. Find T such that F3 = 0 for all U


# Hannah: debugging
# def print_values(f):
# 	for v in vertices(mesh):
# 	    x = v.point().x()
# 	    y = v.point().y()
# 	    print(x, y, f(x, y))

# print_values(u)
# print_values(ulast)
# print_values(p)
# print_values(T)

# prm = solver.parameters	
# prm["newton_solver"]["absolute_tolerance"] = 1E-6
# prm["newton_solver"]["relative_tolerance"] = 1E-6
# prm["newton_solver"]["maximum_iterations"] = 50
# prm["newton_solver"]["relaxation_parameter"] = 0.1
# prm["newton_solver"]["preconditioner"] = "ilu"

# SRTD algorithm
iters = 0; maxiters = 1; 
while iters < maxiters:

	print("SRTD iteration: " + str(iters))
	print("Unified Stokes: solve for u and pi")

	# u = 0, pi = 0
	u = Function(velocity)
	pi = Function(pressure)

	# F1 = nu*inner(grad(u), grad(v))*dx + nu*r*div(u)*div(v)*dx + dot(dot(u, grad(u)), v)*dx - pi*div(v)*dx - F(f, ulast, p, pi, T)*dx
	# J1 = derivative(F1, u)

	# problem1 = NonlinearVariationalProblem(F1, u, bcs, J=J1)
	# solver1  = NonlinearVariationalSolver(problem1)

	# prm = solver1.parameters	
	# prm["newton_solver"]["absolute_tolerance"] = 1E-6
	# prm["newton_solver"]["relative_tolerance"] = 1E-6


	# Unified Stokes algorithm
	usaiters = 0; maxusaiters = 10; divunorm = 1
	while usaiters < maxusaiters and divunorm > 1e-10:

		print("USA iteration: " + str(usaiters))

		F1 = nu*inner(grad(u), grad(v))*dx + nu*r*div(u)*div(v)*dx + dot(dot(u, grad(u)), v)*dx - pi*div(v)*dx - F(f, ulast, p, pi, T)*dx
		J1 = derivative(F1, u)

		# But we want to use ulast as initial guess
		problem1 = NonlinearVariationalProblem(F1, u, bcs, J1)
		solver1  = NonlinearVariationalSolver(problem1)

		# Solve for u and update pi	
		solver1.solve()
		divu = project(div(u), pressure)
		pi.vector().axpy(-r, divu.vector())

		# Alternative update
		# tmp = Function(pressure)
		# tmp.vector().update(pi.vector() + r * (divu.vector()));
		# pi = tmp;

		# maybe need to reassemble at each level because pi changes??

		# Check stopping condition
		divunorm = sqrt(assemble(div(u)*div(u)*dx(mesh)))
		print(divunorm)
		usaiters += 1

	# Update past u solution
	ulast.assign(u)

	print("Solving for p")

	# Solve for p
	F2 = (p*(q - lambda1*dot(u, grad(q))))*dx - pi*q*dx
	# this is not nonlinear in p
	J2 = derivative(F2, p)

	problem2 = NonlinearVariationalProblem(F2, p, J=J2)
	solver2  = NonlinearVariationalSolver(problem2)
	
	solver2.solve()
	# solve(F2 == 0, p)

	print("Solving for T")

	# Solve for T
	F3 = inner(T, (U - lambda1*dot(u, grad(U))))*dx + inner(lambda1*M(u, T), U)*dx - 2*nu*inner(E(u), U)*dx
	J3 = derivative(F3, T)

	problem3 = NonlinearVariationalProblem(F3, T, J=J3)
	solver3  = NonlinearVariationalSolver(problem3)

	solver3.solve()
	# solve(F3 == 0, T)

	# Check stopping condition
	iters += 1

# plot the solutions
file = File("srtd/velocity.pvd")
file << u

file = File("srtd/pressure.pvd")
file << p

file = File("srtd/stress.pvd")
file << T


# try maybe not providing J
# try Taylor-Hood?
# try just one solve
# reasons Newton's Method wouldn't converge
# how can we see what the problem looks like?
# updated Navier-Stokes equations

# Define bilinear forms
# F1 = nu*inner(grad(u), grad(v))*dx + nu*r*div(u)*div(v)*dx + dot(dot(u, grad(u)), v)*dx - pi*div(v)*dx - F(f, ulast, p, pi, T)*dx
# F2 = (p*(q - lambda1*dot(u, grad(q))))*dx - pi*q*dx
# F3 = inner(T, (U - lambda1*dot(u, grad(U))))*dx + inner(lambda1*M(u, T), U)*dx - 2*nu*inner(E(u), U)*dx

# F1 without penalty term
# F1 = nu*inner(grad(u), grad(v))*dx + dot(dot(u, grad(u)), v)*dx - pi*div(v)*dx - F(f, ulast, p, pi, T)*dx

# Alternative F2 form
# F2 = (p + lambda1*dot(u, grad(p)))*q*dx - pi*q*dx 





