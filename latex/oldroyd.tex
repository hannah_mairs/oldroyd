\documentclass[10pt]{article}

\usepackage[margin=1.25in]{geometry}

\input{preamble.tex}

\begin{document}

The goal of this project is to study certain computational aspects of non-Newtonian fluid models.
Recently, there have been mathematical advances that have made it possible to develop new solution algorithms to these models in the finite element framework. 
Among these are formulations for equations that are subsets of Oldroyd models including upper- and lower-convected Maxwell models. 
Throughout this work, we will focus on the subset of Oldroyd models that involve three parameters: the kinematic fluid viscosity $\eta$ and two rheological parameters $\lambda_1$ and $\mu_1$. 
Questions of interest are outlined in \ref{sec:questions}. 

\section{Introduction} \label{sec:intro}

Let $\mathcal{D} \subset \Reyuls^d$ be in either two or three dimensions, $d = 2, 3$. 
Time-independent fluid models can be written as
\begin{align} \label{eq:fluid-model}
\uu \cdot \nabla \uu + \nabla p = \sdiv \TT + \ff
\end{align}
where $\uu$ is the fluid velocity, $p$ the pressure, $\TT$ is the extra stress, and $\ff$  a forcing function. 
The models differ in how the stress $\TT$ depends on velocity $\uu$. 


 
 
\section{Three parameter Oldroyd models} \label{sec:oldroyd}
The stress for three parameter Oldroyd models takes the form 
\begin{align}
\TT+  \lambda_1(\uu \cdot \nabla \TT + \RR \circ \TT + \TT \circ \RR^t) - \mu_1(\EE \circ \TT + \TT \circ \EE) = 2 \eta \EE,
\end{align}
where 
$$\RR = \half (\nabla \uu^t - \nabla \uu) \text{ and } \EE = \half (\nabla \uu + \nabla \uu^t)$$
and $\circ$ is normal matrix multiplication. Other Oldroyd parameters $\lambda_2$, $\mu_0$, $\mu_2$, $\nu_0$, and $\nu_1$ are set to zero. We get the upper and lower convected Maxwell models by letting $\lambda_1 = \mu_1$ and $\lambda_1 = -\mu_1$, respectively. The full incompressible steady model then becomes
\begin{align} \label{eq:3-parm-oldroyd}
\begin{split}
\uu \cdot \nabla \uu + \nabla p &= \sdiv \TT + \ff \text{ in } \mathcal{D}, \\
\sdiv \uu = 0 \text{ in } &\mathcal{D}, \quad \uu = \mathbf{0} \text{ on } \partial \mathcal{D}, \\
\TT+  \lambda_1(\uu \cdot \nabla \TT + \RR \circ \TT + \TT \circ \RR^t) &- \mu_1(\EE \circ \TT + \TT \circ \EE) = 2 \eta \EE \text{ in } \mathcal{D}. 
\end{split}
\end{align}

By applying derived identities and defining an auxiliary pressure function $\pi = p + \lambda_1 \uu \cdot \nabla p$,
we can restate the system \eqref{eq:3-parm-oldroyd} as
\begin{align} \label{eq:3-param-new}
\begin{split}
-\eta \Delta \uu + \uu \cdot \nabla \uu + \nabla \pi &= \mathcal{F}(\ff, \uu, p, \TT)\text{ in } \mathcal{D}, \\
\sdiv \uu = 0 \text{ in } &\mathcal{D}, \quad \uu = \mathbf{0} \text{ on } \partial \mathcal{D}, \\
p + \lambda_1 &\uu \cdot \nabla p = \pi \\
\TT + \lambda_1(\uu \cdot \nabla \TT - (\nabla \uu) \circ \TT - \TT \circ (\nabla \uu^t))& + 
(\lambda_1 - \mu_1)(\EE \circ \TT + \TT \cdot \EE) = 2 \eta \EE \text{ in } \mathcal{D}
\end{split}
\end{align}
where $\mathcal{F}$ is given by
\begin{align}
\begin{split}
\mathcal{F}(\ff, \uu, p, \TT) &= \ff + \lambda_1 \uu \cdot \nabla \ff + \lambda_1(\nabla \uu)^t \nabla p - \lambda_1(\uu \cdot \nabla (\uu \cdot \nabla \uu)) - \sdiv ((\nabla \uu) \circ \TT)) \\
&-(\lambda_1 - \mu_1)\nabla \cdot (\EE \circ \TT + \TT \circ \EE).
\end{split}
\end{align}

Solutions to the three parameter Oldroyd subset have been shown to exist by way of an iterative modification of \eqref{eq:3-param-new}. A variational formulation of the iterative scheme has been derived, which we will use as a computational solution method via finite elements. The method is called Selective Replacement of the Tensor Divergence (SRTD) and outlined below.

\begin{enumerate}
\item Let $\uu^0 = \mathbf{0}, p^0 = 0,$ and $\TT^0 = \mathbf{0}$ in some domain $\mathcal{D}$ with external force $\ff$.

\item Knowing $\uu^n, p^n,$ and $\TT^n$ and for function spaces $V$ and $\Pi$, find $\uu^{n+1} \in V$ and $\pi^{n+1} \in \Pi$ such that
\begin{align*}
\eta \int_\mathcal{D} \nabla \uu^{n+1} : \nabla \vv dx &+ \int_\mathcal{D}(\uu^{n+1} \cdot \nabla \uu^{n+1}) \cdot \vv dx - \int_\mathcal{D} \pi^{n+1} \nabla \cdot \vv dx = \int_\mathcal{D} \mathcal{F}(\ff, \uu^n, p^n, \TT^n)\cdot \vv dx \\
&= \int_\mathcal{D} \ff \cdot (\vv - \lambda_1 \uu^n \cdot \nabla \vv)dx - \lambda_1 \big(\int_\mathcal{D} p^n(\nabla \uu^n)^t : \nabla \vv dx \\
&- \int_\mathcal{D} (\uu^n \cdot \nabla \uu^n) \cdot (\uu^n \cdot \nabla \vv)dx + \int_\mathcal{D} (\nabla \uu^n \circ \TT^n):\nabla \vv dx) \\
&+ (\lambda_1 - \mu_1)\int_\mathcal{D} (\EE(\uu^n) \circ \TT^n + \TT^n \circ \EE(\uu^n)) : \nabla \vv dx \quad \forall \vv \in V.
\end{align*}
We use the Unified Stokes algorithm to solve for $\pi^{n+1}$ above.

\item Solve the transport problem for $p^{n+1}$: Find $p^{n+1} \in \widehat{\Pi}$ such that
\begin{align*}
\int_\mathcal{D} p^{n+1}(v - \lambda_1 \uu^{n+1} \cdot \nabla v) dx = \int_\mathcal{D} \pi^{n+1} v dx \quad \forall v \in \widehat{\Pi}.
\end{align*}

\item Solve the transport problem for $\TT^{n+1}$: Find $\TT^{n+1} \in \widetilde{\Pi}^{d^2}$ such that
\begin{align*}
\int_\mathcal{D} \TT^{n+1} : (\UU - \lambda_1 \uu^{n+1} \cdot \nabla \UU)dx + \int_\mathcal{D} \mathcal{M}(\uu^{n+1}) \TT^{n+1} : \UU dx \\
= 2 \eta \int_\mathcal{D} \EE(\uu^{n+1}) : \UU dx \quad \forall \UU \in \widetilde{\Pi}^{d^2}.
\end{align*}
Above, $\mathcal{M}(\uu) \TT = \RR \circ \TT + \TT \circ \RR^t - \mu_1 (\EE \circ \TT + \TT \circ \EE)$, but check with Ridg to be sure.
% NOT SURE IF THIS IS RIGHT

\item Repeat from 2. until stopping conditions are met.
\end{enumerate} 



%\section{Grade-two models} \label{sec:grade-two}

\section{Questions} \label{sec:questions}

\begin{itemize}

\item We will implement the SRTD algorithm in the finite element software FEniCS under a variety of conditions. Let $\uu_O$ be our Oldroyd solution, $\uu_N$ a Navier-Stokes solution, and $\uu_G$ a Grade-two model solution. The FEniCS implementation can be checked for correctness by comparisons to Navier-Stokes and Grade-two models since 
\begin{equation}
\uu_O \rightarrow \uu_N {\text{ as }} \lambda_1 \rightarrow 0,
\end{equation}
\begin{equation}
\uu_G \rightarrow \uu_N {\text{ as }} \alpha \rightarrow 0,
\end{equation}
and
\begin{equation}
\uu_O \rightarrow \uu_G {\text{ as }} \lambda_1 \rightarrow 0.
\end{equation}

\item How do Oldroyd and grade-two models differ for larger $\lambda_1$?

\item Compare SRTD with the Explicitly Elliptic Momentum Equations (EEME) method convected Maxwell models.

\item We can compute approaches to the five parameter Oldroyd model as $\lambda_2, \mu_2 \rightarrow 0$ and compare with our three parameter case where $\lambda_2 = \mu_2 = 0$. 

\end{itemize}

\section{Other fluid models} \label{other-fluid-models}

\subsection{Navier-Stokes} \label{navier-stokes}
 
 For an incompressible Newtonian fluid with $\sdiv \uu = 0$,
 \begin{align}
 \TT = \eta(\nabla \uu + \nabla \uu^t)
 \end{align}
 so that $\sdiv \TT = \eta \Delta \uu$ and  \eqref{eq:fluid-model} reduces to 
\begin{align}  \label{eq:ns-eqtns}
\begin{split}
-\Delta \uu + \nabla p &= -R(\uu \cdot \nabla \uu) \\
\sdiv \uu &= 0
\end{split}
\end{align} 
with Reynolds number $R$. For us, $R= 1/\eta$. 
The variational form for \eqref{eq:ns-eqtns} takes the form: Find $\uu \in V - \gamma$ and $p \in \Pi$ such that 

\begin{align} \label{eq:ns-var}
\begin{split}
\int_\mathcal{D} \nabla \uu : \nabla \vv \, dx - \int_\mathcal{D} \nabla \vv \, p \, dx + R\int_\mathcal{D} (\uu \cdot \nabla \uu) \cdot \vv \, dx = 0  \quad \forall \vv \in V \\
\int_\mathcal{D} \nabla \uu \, q  \, dx= 0 \quad  \forall q \in \Pi.
\end{split}
\end{align}
There are a number of solution methods for the Navier-Stokes equations. Here, we will employ an iterative method time-independent solver  that introduces the bilinear form 
\begin{equation}
\hat{c}(\uu, \ww, \vv) = c(\uu, \ww, \vv) + c(\ww, \uu, \vv)
\end{equation}
where
\begin{equation}
c(\uu, \vv, \ww) := \int_\mathcal{D} (\uu \cdot \nabla \vv) \cdot \ww \, dx.
\end{equation}
Then the method becomes: Find $\ww \in Z$ such that
\begin{align} \label{eq:ns-new-form}
\begin{split}
\int_\mathcal{D} \nabla \ww : \nabla \vv \, dx + R\hat{c}(\uu^n, \ww, \vv) &= F(\uu^n, \vv) \quad \forall \vv \in Z \\
{\text{Set }} \uu^{n+1} &= \uu^n - \ww.
\end{split}
\end{align}

We can solve \eqref{eq:ns-new-form} using the iterated penalty method to enforce the incompressibility with initial guess $\uu^0$ from a Stokes problem in $\mathcal{D}$ with the same boundary conditions. We will call the Navier-Stokes solution $\uu_N$. To test this implementation, we can compare the solution against a variety of well known Navier-Stokes solution algorithms. 


\subsection{Grade-two models} \label{grade-two}


\section{FEniCS implementation} \label{fenics}


\end{document}
